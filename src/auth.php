<?php

/**
* The Race Nation Auth Package
*/

namespace RaceNation\Auth;

use RaceNation\Auth\Exception as Exception;
use \JWT as JWT;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Auth
{
  
  /**
   * Properties
   */

// Settable //

  /**
   * Shared Secret
   * @var string / null
   */
  private $sharedSecret = null;

  /**
   * JSON Web Token
   * Must be a valid token
   * @var string / null
   */
  private $jwt = null;

  /**
   * Audience
   * Must be an RFC compliant URI
   * @var string / null
   */
  private $aud = null;

  /**
   * Subject
   * Must be an RFC compliant URI
   * @var string / null
   */
  private $sub = null;

  /**
   * Issuer (username)
   * Must be an RFC compliant email address
   * @var string / null
   */
  private $iss = null;

  /**
   * Password
   * @var string / null
   */
  private $pwd = null; // Pwd

  /**
   * Package debug mode switch
   * @var boolean
   */
  private $debug = false;

// Immutable //

  /**
   * The issued at time.
   * @var integer
   */
  private $iat = null;

  /**
   * The expiry timestamp
   * @var integer
   */
  private $exp = null;

  /**
   * The base URL for all API requests
   * @var string
   */
  private $baseUri = 'https://auth-race-nation-com.herokuapp.com';

  /**
   * The Guzzle Client Instance
   * @var GuzzleHttp\Client;
   */
  private $client = null;

  /**
   * Constructor
   * @param string  $sharedSecret Shared Secret
   * @param string  $jwt          JSON Web Token
   * @param string  $aud          Audience
   * @param string  $sub          Subject
   * @param string  $iss          Issuer (username)
   * @param string  $pwd          Password
   * @param boolean $debug        Package debug mode switch
   */
  public function __construct($jwt=null, $sharedSecret=null, $sub=null, $iss=null, $pwd=null, $aud=null, $debug=false)
  {
    $this->setSharedSecret($sharedSecret);
    $this->setJWT($jwt);
    $this->setAUD($aud);
    $this->setSUB($sub);
    $this->setISS($iss);
    $this->setPWD($pwd);
    $this->debug = $debug;

    $this->iat = time();
    $this->exp = time() + 60;
    $this->client = new Client(['base_uri' => $this->baseUri]);
    
    return $this;
  }

/**
 * Public Methods
 */

  /**
   * Setter for the shared secret or key
   * @param string $sharedSecret
   */
  public function setSharedSecret($sharedSecret)
  {
    $this->sharedSecret = $sharedSecret;
  }

  /**
   * Setter for the JWT property
   * @param string $jwt
   */
  public function setJWT($jwt)
  {
    $this->jwt = $jwt;
  }

  /**
   * Setter for the Audience property
   * @param string $aud
   */
  public function setAUD($aud)
  {
    if (is_null($aud)) {
      $aud = 'https://race-nation.com';
    }
    if(is_null($aud) || filter_var($aud, FILTER_VALIDATE_URL)){ 
      $this->aud = $aud;
    } else {
      throw new Exception("Invalid AUD $aud . AUD Must be an RFC compliant URI.", 1);
    }
  }

  /**
   * Setter for the Subject property
   * @param string $sub
   */
  public function setSUB($sub)
  {
   
    if(is_null($sub) || filter_var($sub, FILTER_VALIDATE_URL)){ 
      $this->sub = $sub;
    } else {
      throw new Exception("Invalid SUB $sub . SUB Must be an RFC compliant URI.", 1);
    }
  }

  /**
   * Setter for the Issuer property
   * @param string $iss
   */
  public function setISS($iss)
  {
    if(is_null($iss) || filter_var($iss, FILTER_VALIDATE_EMAIL)){ 
      $this->iss = $iss;
    } else { 
      throw new Exception("Invalid ISS $iss . ISS MUST be an RFC compliant email address.", 1);
    }     
  }

  /**
   * Setter for the password property
   * @param string $pwd
   */
  public function setPWD($pwd)
  {
    $this->pwd = $pwd;
  }

  /**
   * Getter for the JWT property
   * @return string JWT
   */
  public function getJWT()
  {
    return $this->jwt;
  }

  /**
   * Getter for the ISS property
   * @return string ISS
   */
  public function getISS()
  {
    return $this->iss;
  }

  /**
   * Main Auth Method - 
   * attempts to authenticate a user name (iss) and password (pwd) and return a 
   * user JWT if successful
   * @return string  a full, encoded which can be used for future verify calls
   */
  public function auth()
  {

    $payload = array(
      "aud" => $this->aud,
      "sub" => $this->sub,
      "iat" => $this->iat,
      "exp" => $this->exp,
      "iss" => $this->iss,
      "pwd" => $this->pwd,
    );

    $this->jwt = JWT::encode($payload, $this->sharedSecret);

    try {
      $response = $this->client->post('/auth', [
        'body' => json_encode($payload),
        'query' => ['jwt' => $this->jwt]
      ]);

      $response = json_decode($response->getBody(), true);
      
      // Validate the JWT
      if ($this->isValidJWT($response['verifyJwt'])) {
        $this->exp = $response['payload']['exp'];
        $this->iat = $response['payload']['iat'];
        $this->jwt = $response['userJwt'];
        $this->pwd = null;
        $this->sharedSecret = null;
        return $this->jwt;
      } else {
        throw new Exception("Response JWT Invalid. Cannot proceed.", 1);
      }

    } catch (ClientException $e) {
      throw $e;
    }

  }


  /**
   * The main verify call
   * @return boolean
   */
  public function verify()
  {
    
    if (!$this->readyForVerify()) {
      throw new Exception("Auth not ready for verify. Check required properties are set.", 1);
    }

    try {
      $response = $this->client->get('/verify', [
        'query' => ['jwt' => $this->jwt]
      ]);

      if ($response->getStatusCode() == 200) {
        $response = json_decode($response->getBody(), true);
        $this->iss = $response['payload']['iss'];
        $this->exp = $response['payload']['exp'];
        $this->iat = $response['payload']['iat'];
        $this->pwd = null;
        $this->sharedSecret = null;
        return true;  
      }

    } catch (ClientException $e) {
      // And do nothing
      throw $e;
      
    }

    return false;

  }

/**
 * Private Methods
 */

  /**
   * Check the object state is ready for an auth call
   * @return boolean
   */
  private function readyForAuth()
  {

    if (is_null($this->sharedSecret)) {
      throw new Exception("Shared Secret not supplied. Cannot proceed", 1);
    }

    if (is_null($this->aud)) {
      throw new Exception("AUD not supplied. Cannot proceed", 1);
    }

    if (is_null($this->iss)) {
      throw new Exception("ISS not supplied. Cannot proceed", 1);
    }

    if (is_null($this->pwd)) {
      throw new Exception("PWD not supplied. Cannot proceed", 1);
    }

    if (is_null($this->sub)) {
      throw new Exception("SUB not supplied. Cannot proceed", 1);
    }

    return true;

  }

  /**
   * Check the object is ready for a verify call
   * @return boolean
   */
  private function readyForVerify()
  {
   
    if (is_null($this->jwt)) {
      throw new Exception("JWT not supplied. Cannot proceed", 1);
    }

    if (!$this->isValidJWT($this->jwt)) {
      throw new Exception("Provided JWT Invalid. Cannot proceed.", 1);
    }

    return true;
  }

  /**
   * Validates a given JWT with an optional key
   * @param  string  $jwt          The JWT to validate
   * @param  string  $sharedSecret the key to use in validation 
   *                               (if null no validation occurs)
   * @return boolean
   */
  private function isValidJWT($jwt, $sharedSecret = null) {
    try {
      JWT::decode($jwt, $sharedSecret, array('HS256'));
      return true;
    } catch (Exception $e) {
      return false;
    }
  }
  
}