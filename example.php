<?php

use RaceNation\Auth\Auth;

$key = 'Shared Secret Key for the Subscriber';
$rnAuth = new Auth($key, 'https://test.race-nation.com', 'test@race-nation.com', 'TestPass123' );
$rnAuth->auth();

// Store the JWT in a veriable, persists to session or DB
$myJWT = $rnAuth->getJWT();

// Try a verify call
if ($rnAuth->verify()) {
  echo 'OK';
} else {
  echo 'Error - Unauthorized';
}

// Example 2 - using a stored JWT to populate the verification step

// Try verifying from the JWT we stored earlier
$rnAuth->setSharedSecret($key);
$rnAuth->setJWT($myJWT);
if ($rnAuth->verifyFromJWT()) {
  echo 'OK';
} else {
  echo 'Error - Unauthorized';
}