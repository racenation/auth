# Race Nation Auth Package

The Race Nation Authentication Package

## Installation

Add the following to your composer.json:

    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:racenation/auth.git"
        }
    ]


Then add the following to your require line:

    "racenation/auth": "dev-master"

And run **composer update**

## Usage

This is a fully encapsulated package but for internals and bug fixing read the source!

This package is fully namespaced so be sure to declare:

    use RaceNation\Auth\Auth as RnAuth;

Basic usage is as follows:

### Initial Auth

Instantiate a new instance of the package passing in the following arguments:

    $rnAuth = new RnAuth(null, <sharedSecret>, <subject>, <userName>, <password>);

(**Note**: the key in this instance is the system's shared secret (the sub-key) and **not** a user shared secret)

(**Note**: the first argument must be null in an auth scenario)

Then call the auth method:

    $rnAuth->auth();

This will either return a user JWT for the authenticated user or error out with an exception. 

The JWT returned can be persisted in your session for as long as the expiry time stamp is valid (by default 86,400 seconds or 24 hours).

### Verify

You can reuse the object once authenticated to make further verification calls using the following method call:

    ...

    if ($rnAuth->verify()) {
      echo 'OK';
    } else {
      echo 'Error - Unauthorised';
    }

    ...

You can also pass in a response JWT to populate the object without making an additional auth() call:

    ...

    $rnAuth->setJWT($userJWT);
    if ($rnAuth->verify()) {
      echo 'OK';
    } else {
      echo 'Error - Unauthorised';
    }

    ...

## Requirements (included)

    {
      "php": ">=5.3.0",
      "guzzlehttp/guzzle": "~6.0",
      "firebase/php-jwt": "^2.2"
    }